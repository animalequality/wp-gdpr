<?php

/**
 * (c) 2018 Raphaël Droz <raphael.droz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 *
 * When run via `composer update|install` arguments can only be set from environment.
 * Otherwise `composer run-script downloadArtifacts -- --project=xxx --...`
 *
 */

namespace AnimalEquality\CI;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\BufferedOutput;

require_once(__DIR__ . '/ArtifactDownload.php');

class Installer {
    public static function postInstallCmd(Event $event) {
        $composer = $event->getComposer();
        $io = $event->getIO();

        $dl = new ArtifactDownload($composer, $io);
        $definition = $dl->getDefinition();

        $args = $event->getArguments();
        $cmd_args = array_merge(['artifact:gitlab:download'], $args);

        $input = new ArgvInput($cmd_args, $definition);
        $output = new BufferedOutput();
        $dl->run($input, $output);
    }
}
