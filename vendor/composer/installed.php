<?php return array(
    'root' => array(
        'name' => 'animalequality/gdpr',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '6ff8c34835c26b565b22b0bfc0d028596ba5bc68',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'animalequality/gdpr' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '6ff8c34835c26b565b22b0bfc0d028596ba5bc68',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'squizlabs/php_codesniffer' => array(
            'pretty_version' => '3.8.0',
            'version' => '3.8.0.0',
            'reference' => '5805f7a4e4958dbb5e944ef1e6edae0a303765e7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../squizlabs/php_codesniffer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
    ),
);
