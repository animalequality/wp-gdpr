/**
 * Provides the markup for overlaying external content
 * @param {string} source
 * @returns {string}
 */
const createEmbedConsentOverlayMarkup = (source, embedConfig, embedClientHeight) => {
  const element = document.createElement('div');
  const id = parseInt(Math.random() * 10000);
  const overlayConfig = embedConfig.overlay;
  const message = overlayConfig.message.text.replace(
    '{link}',
    `<a href="${overlayConfig.message.link.url}" target="_blank">${overlayConfig.message.link.label}</a>`
  ).replace(
    '{source}',
    source.name
  );

  element.classList.add('no-cookie-consent-overlay');

  if (embedClientHeight > 250) {
    element.classList.add('no-cookie-consent-overlay--absolute');
  }

  element.innerHTML = `<div>
    <p class="m-0 color-white align-center">
      ${message}
    </p>
    <button class="${overlayConfig.button.cssClasses} no-cookie-consent-overlay__unlock">
      ${overlayConfig.button.label}
    </button>
    <form class="mt-2">
      <input type="checkbox" id="cookie-consent-all-${id}" />
      <label for="cookie-consent-all-${id}" class="color-white">
        ${overlayConfig.checkbox.label}
      </label>
    </form>
  </div>`;

  return element;
};

function embedHasConsent(source) {
  return source.dependencies.filter(dependency => this.hasConsent(dependency)).length === source.dependencies.length;
};

/**
 * Blocks an embed by adding the layer if available and removing the iframe src
 * @returns {void}
 */
function blockEmbed(embed, source) {
  const overlay = createEmbedConsentOverlayMarkup(source, this.embedConfig, embed.clientHeight);
  const embedParent = embed.parentElement;
  let overlayParent;

  if (embed.dataset.isBlocked === 'true') {
    return;
  }

  if (source.overlayContainer) {
    overlayParent = embed.closest(source.overlayContainer);
  } else if (embedParent.classList.contains('wp-block-embed__wrapper')) {
    overlayParent = embedParent;
  } else {
    overlayParent = document.createElement('div');

    overlayParent.style.position = 'relative';
    overlayParent.classList.add('no-cookie-consent-overlay-container');
    overlayParent.style.height = '100%';
    embedParent.insertBefore(overlayParent, embed);
    overlayParent.appendChild(embed);
  }

  overlayParent.appendChild(overlay);
  embed.dataset.isBlocked = true;

  if (embed.getAttribute('src')) {
    embed.dataset.src = embed.getAttribute('src');
    embed.removeAttribute('src');
  }

  overlay.querySelector('.no-cookie-consent-overlay__unlock').addEventListener('click', () => {
    if (overlay.querySelector('input[type="checkbox"]').checked) {
      this._consentToSources(source.dependencies);
    }

    unblockEmbed(embed, source);
    dispatchEmbedConsent(source.id);
  });
};

/**
   * Unblocks an embed by removing the layer if available and setting the iframe src
   * @returns {void}
   */
function unblockEmbed(embed, source) {
  let overlay = null;

  if (source.overlayContainer) {
    overlay = embed.closest(source.overlayContainer).querySelector('.no-cookie-consent-overlay');
  } else {
    overlay = embed.parentElement.querySelector('.no-cookie-consent-overlay');
  }

  if (overlay) {
    overlay.remove();
  }

  if (embed.dataset.src) {
    embed.setAttribute('src', embed.dataset.src.replace(/youtube\.com/, 'youtube-nocookie.com'));
    embed.removeAttribute('data-src');
  }
}

/**
 * Dispatches the embed consent given event on window
 * @returns {void}
 */
function dispatchEmbedConsent(type) {
  const event = new window.Event(`aePrivacyConsentEmbed${type[0].toUpperCase() + type.slice(1)}`);
  window.dispatchEvent(event);
}

/**
  * Iterates through embeds and blocks/unblocks them based upon the current consents
  * @returns {void}
  */
function handleEmbeds() {
  this.embedConfig.sources.forEach(source => {
    const embeds = document.querySelectorAll(source.embedElement);

    for (const embed of embeds) {
      if ((embed.dataset.src && embed.dataset.src.includes(source.id)) || (embed.src && embed.src.includes(source.id))) {
        if (embedHasConsent.call(this, source)) {
          unblockEmbed(embed, source);
        } else {
          blockEmbed.call(this, embed, source);
        }
      }
    }

    if (this.hasConsent(source.id)) {
      dispatchEmbedConsent(source.id);
    }
  });
}

export {handleEmbeds};
