import ready from 'document-ready';
import PrivacyConsentManager from './PrivacyConsentManager.js';

import '../style/style.scss';

ready(() => {
  window.privacyConsentManager = new PrivacyConsentManager();
  window.privacyConsentManager.init();
});
