<div
    class="cookie-consent-manager"
    data-google-tag-manager-config='<?php echo esc_html(json_encode($this->config['googleTagManager'])); ?>'
    data-embed-config='<?php echo esc_html(json_encode($this->config['embeds'])); ?>'
>
    <div class="cookie-consent-manager__inner">
        <div class="cookie-consent-manager__headline">
            <?php echo !empty($this->config['title']) ? esc_html($this->config['title']) : ''; ?>
        </div>

        <div class="cookie-consent-manager__buttons">
            <?php if ($this->config['controls']['position'] === 'top') {
                implode(array_map(function ($button) {
                    include 'control.php';
                }, $this->config['controls']['buttons']));
            } ?>
        </div>

        <p class="cookie-consent-manager__description">
            <?php echo $this->config['description']; ?>
        </p>

        <div class="cookie-consent-manager__settings">
            <form>
                <table>
                    <?php implode(array_map(function ($source) {
                        include 'source-input.php';
                    }, $this->config['sources'])); ?>
                </table>
            </form>
        </div>

        <div class="cookie-consent-manager__buttons">
            <?php if ($this->config['controls']['position'] === 'bottom') {
                implode(array_map(function ($button) {
                    include 'control.php';
                }, $this->config['controls']['buttons']));
            } ?>
        </div>

        <?php if (!empty($this->config['imprint'])) { ?>
            <div class="cookie-consent-manager__imprint">
                <a href="<?php echo $this->config['imprint']['url']; ?>" target="_blank">
                    <?php echo esc_html($this->config['imprint']['label']); ?>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
