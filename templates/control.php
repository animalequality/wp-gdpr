<button class="<?php echo esc_html($button['cssClasses']) ?? ''; ?>" data-action="<?php echo esc_html($button['action']); ?>">
    <?php echo esc_html($button['label']); ?>
</button>
