<?php

namespace AnimalEquality\WP;

/**
 * Plugin Name: Consent Manager
 * Plugin URI: https://gitlab.com/animalequality/wp-gdpr
 * Description: Consent Manager (aka cookie banner)
 * Version: 5.2.1
 * Author: Robin Schuelein, Raphaël Droz
 * Author URI: https://gitlab.com/animalequality
 * License: GPL-3.0
 */

class AeConsentManager
{
    private $config;

    public function __construct()
    {
        add_action('init', [$this, 'init']);
    }

    public function init()
    {
        if (!function_exists('yaml_parse_file') || !defined('AE_COUNTRY') || empty(AE_COUNTRY)) {
            return;
        }

        $this->config = @\yaml_parse_file(sprintf(
            '%s/config/%s/%s.yaml',
            __DIR__,
            strtolower(AE_COUNTRY),
            str_replace(' ', '', wp_get_theme()->get('Name'))
        ));

        if (empty($this->config)) {
            error_log('GDPR: no configuration provided');
            return;
        }

        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_action('wp_footer', [$this, 'render_banner']);
        add_action('admin_menu', [$this, 'add_tool_menu_item']);
        add_action('rest_api_init', function () {
            register_rest_route('ae-consent-manager/v1', '/update-statistics', array(
                'methods' => 'POST',
                'callback' => [$this, 'rest_update_statistics'],
                'permission_callback' => '__return_true',
            ));

            register_rest_route('ae-consent-manager/v1', '/get-statistics', array(
                'methods' => 'GET',
                'callback' => [$this, 'rest_get_statistics'],
                'permission_callback' => function () {
                    return current_user_can('edit_others_posts');
                }
            ));
        });

        add_filter('ae_gtm_script', [$this, 'filter_gtm_tag'], 10);
        add_filter('the_content', [$this, 'the_content'], 10);
    }

    public function enqueue_scripts()
    {
        $js_path = '/dist/index.js';
        $css_path = '/dist/index.css';

        if (file_exists(plugin_dir_path(__FILE__) . $js_path)) {
            wp_register_script('ae-gdpr-script', plugins_url($js_path, __FILE__), [], filemtime(plugin_dir_path(__FILE__) . $js_path));
            wp_enqueue_script('ae-gdpr-script');
        }

        if (file_exists(plugin_dir_path(__FILE__) . $css_path)) {
            wp_enqueue_style('ae-gdpr-style', plugins_url($css_path, __FILE__), [], filemtime(plugin_dir_path(__FILE__) . $css_path));
        }
    }

    public function filter_gtm_tag($script)
    {
        return sprintf(
            'function loadGoogleTagManager() {%s};
            window.addEventListener("aePrivacyConsentGiven", function() {
                if (window.privacyConsentManager.hasGoogleTagManagerConsent() && !window.privacyConsentManager.googleTagManagerLoaded) {
                    loadGoogleTagManager();
                    window.privacyConsentManager.googleTagManagerLoaded = true;
                }
            });',
            $script
        );
    }

    public function the_content($content)
    {
        if (empty(trim($content))) {
            return $content;
        }

        $tags = new \WP_HTML_Tag_Processor($content);

        while ($tags->next_tag()) {
            if (in_array($tags->get_tag(), ['IFRAME', 'IMG'])) {
                foreach ($this->config['embeds']['sources'] as $embed_source) {
                    if (strpos($tags->get_attribute('src'), $embed_source['id']) !== false) {
                        $tags->set_attribute('data-src', $tags->get_attribute('src'));
                        $tags->set_attribute('src', '');
                    }
                }
            }
        }

        return $tags->get_updated_html();
    }

    public function render_banner()
    {
        include __DIR__ . '/templates/banner.php';
    }

    public function rest_update_statistics(\WP_REST_Request $request)
    {
        try {
            $sources = array_merge(array_map(fn($source) => $source['id'], $this->config['sources']), ["sessions"]);
            $consents = $request->get_json_params();
            $consentLog = [];
            $blockedLog = [];

            if (!is_array($consents)) {
                $message = 'Invalid input. Expecting an array of strings.';

                error_log(sprintf('ae-gdpr statistics: %s Received %s', $message, json_encode($consents)));

                return new \WP_REST_Response(['error' => $message], 400);
            }

            $statistics = get_option('ae_consent_manager_statistics', []);

            if (!is_array($statistics)) {
                $statistics = [];
            }

            foreach ($sources as $sourceId) {
                if (array_search($sourceId, $consents) === false) {
                    $blockedLog[] = $sourceId;
                    continue;
                }

                $statistics[$sourceId] = 1 + ($statistics[$sourceId] ?? 0);
                $consentLog[] = $sourceId;
            }

            update_option('ae_consent_manager_statistics', $statistics);

            error_log(sprintf('ae-gdpr statistics: Consented=%s | Blocked=%s', implode($consentLog), implode($blockedLog)));

            return new \WP_REST_Response(['message' => 'success'], 200);
        } catch (\Exception $e) {
            error_log(sprintf('ae-gdpr statistics: %s', $e->getMessage()));
        }
    }

    public function rest_get_statistics(\WP_REST_Request $request)
    {
        $statistics = get_option('ae_consent_manager_statistics', []);

        if (!is_array($statistics)) {
            $statistics = [];
        }

        return new \WP_REST_Response($statistics, 200);
    }

    public function add_tool_menu_item()
    {
        add_submenu_page(
            'tools.php',
            'Consent Manager Statistics',
            'Consent Manager Statistics',
            'manage_options',
            'ae-consent-manager-statistics',
            function () {
                wp_redirect('/wp-json/ae-consent-manager/v1/get-statistics?_wpnonce=' . wp_create_nonce('wp_rest'));
            }
        );
    }
}

new AeConsentManager();
